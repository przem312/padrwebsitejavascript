/**
 * Created by przem312 on 2015-05-09.
 */
app.controller('WhoisCtrl', ['$scope', 'whois','appGetter','appSubscriber','$routeParams',
    function($scope, whois,appGetter, appSubscriber,$routeParams)
    {
        $scope.selectedTab = 1;
        whois.getAll()
            .success(function(data)
                {
                    console.log('Success' + data);
                    $scope.whoisData = data;


                    console.log("$scope.applications " +$scope.applications);
                }
            )
            .error(function(data)
            {
                console.log('Error' + data);
                $scope.whoisData = undefined;
            }
            );

        console.log($scope.whoisData);
        $scope.getDeviceData = function (id)
        {
            $scope.applications = [];

            $scope.deviceData = $scope.whoisData[id];
            angular.forEach($scope.deviceData.apps,function(appId){
                var app = appGetter.getApp(appId);
                console.log(app);
                $scope.applications.push(app);
            });
            console.log("Called for id: " + id);
            console.log($scope.deviceData);
        };

        $scope.getAppData = function(appId)
        {
            var appData = appGetter.getApp(appId);
            return appData;
        }

        $scope.subscribe = function(appId,attributeId, freqMs)
        {
            console.log("SUBSCRIBE " + appId + " " + attributeId +" " + freqMs);
            appSubscriber.subscribeOnAttribute(appId,attributeId,freqMs).success(function(data)
            {
                alert("SUCCESS: " + data);
            })
                .error(function(data)
                {
                    alert("ERORR: " + data);
                })
        }
    }
    ]);


// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

app.controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {


});