/**
 * Created by przem312 on 2015-05-12.
 */
app.directive('whoisMain', function()
{
    return {
        restrict: 'CAE',
        scope:
        {
            current: '=current'
        },
        templateUrl: 'whois/whoisMain.html'
    };
});
app.directive('whoisInfo', function()
{
    return {
        restrict: 'E',
        scope:
        {
            current: '=current'
        },
        templateUrl: 'whois/whoisInfo.html',
        controller: function($scope)
        {

        }
    };
});
app.directive('whoisApp', function()
{
    return {
        restrict: 'CAE',

        templateUrl: 'whois/whoisApp.html',
        scope:{
            app: "=app"
            }
    };
});