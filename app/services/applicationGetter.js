/**
 * Created by przem312 on 2015-05-12.
 */
app.factory('appGetter', [ function(){
    var getApp = function(appId){

        /* TODO: ADD MONGODB */
            console.log("called for " + appId);
            var appJson = {
                "id": appId,
                "name": "Undefined",
                "attributes": [
                    {
                        "attrId" : 1,
                        "attrName":"DeviceStatus",
                        "get" : true,
                        "set" : false
                    },
                    {
                        "attrId" : 3,
                        "attrName":"Argument number",
                        "get" : false,
                        "set" : false
                    }
                ]
            }
           return appJson;
    };


    return {getApp: getApp};

}
])