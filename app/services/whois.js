
/**
 * Created by przem312 on 2015-05-09.
 */
app.factory('whois', ['$http', function($http){
    var getAll = function(){
        return $http.get('/REST/STACK/Whois');
};
    var getSingle = function( id ) {
    return getAll().success(function(data){
        return data[id];
    });
    };

    return {
        getAll: getAll,
        getSingle: getSingle
    };
}
    ])