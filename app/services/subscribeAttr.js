/**
 * Created by przem312 on 2015-05-21.
 */
app.factory('appSubscriber', ['$http', function($http) {
    var subscribeOnAttribute = function (appId, attrId, freqMs) {
        var params = {
            url: '/REST/STACK/subscribe',
            method: 'SUBSCRIBE',
            data: {appId: appId, attrId: attrId, time: freqMs}
        }
        console.log(params);
        return $http(params);
    }
    return {subscribeOnAttribute: subscribeOnAttribute};
}
]
)