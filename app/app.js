'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('app', [
  'ngRoute','ui.bootstrap'
]);
app.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/whois/', {
        templateUrl: 'whois/SingleDeviceView.html',
        controller: 'WhoisCtrl'
      }).when('/whois/info/', {
          templateUrl: 'whois/SingleDeviceView.html',
          controller: 'WhoisCtrl'
      }).when('/whois/info/:id', {
          templateUrl: 'whois/SingleDeviceView.html',
          controller: 'WhoisCtrl'
      }).

  otherwise({
  redirectTo: '/'
  });

}])
